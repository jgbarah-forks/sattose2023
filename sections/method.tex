\section{Method}
\label{sec:method}

In this section we present \babia, our version of \codecity using a positioning algorithm for buildings and quarters and our version, still in progress, of the temporal evolution of the city.

% BabiaXR in an nutshell
\subsection{\babia in a nutshell}

\babiaCodeCity is a part of a larger open-source toolset called \babia. The \babia toolset is designed for 3D data visualization in the browser and is available from the npm repository. \babia is based on a framework called \aframe, which allows for the creation of 3D, augmented reality, and virtual reality experiences in the browser. One of the key advantages of using \aframe is that it extends \texttt{HTML} with new entities, allowing for the description of 3D scenes as a part of an \texttt{HTML} document. These scenes can then be manipulated using techniques common to any front-end web developer.

In turn, \aframe is built on top of \tool{Three.js}, which uses the \webgl API provided by modern browsers. \babia extends \aframe by providing components to create visualizations, retrieve data, and manage it with filtering capabilities. Visualizations created with \babia can be displayed on-screen or on VR devices using the facilities provided by browsers. The versatility of \babia, its ability to run in web browsers, and its integration with emerging technologies such as virtual reality and web technologies make it an exciting tool for software visualization. A sample scene created with \babia is shown in \figref{fig:vis_babia}.


\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\columnwidth, keepaspectratio]{img/babia_example.png}
    \caption{Example of a \babia Scene}
    \label{fig:vis_babia}
\end{figure}


% BabiaXR Codecity using spiral
\subsection{\babia CodeCity}

Our implementation of \codecity utilizes a similar approach to the original version in that it maps software metrics to the features of buildings and groups them according to the hierarchical structure of the codebase. However, there are notable differences between our implementation and the original. While the original \codecity was a desktop-based application written in \tool{Smalltalk}, our implementation is built as a browser-based application using \tool{JavaScript}. Additionally, the original version utilized a treemap algorithm for layout, whereas we use a \emph{spiraling algorithm}. With this algorithm, the first building is placed at the center of a spiral and the subsequent buildings are positioned in a spiraling manner around it. This same layout approach is also used for groups of buildings at the same hierarchical level. To better illustrate this approach, \figref{fig:babia_gltf} provides an example of the layout produced by our implementation of \codecity.


\begin{figure}[ht]
    \centering
    \includegraphics[width=0.9\columnwidth, keepaspectratio]{img/city_example.png}
    \caption{Example of a \babiaCodeCity Scene}
    \label{fig:babia_gltf}
\end{figure}

The spiral pattern expands in a clockwise direction, starting from the green quarter located in the center-left of the Figure, and progressing to the next green quarter on its right, followed by the one below it, and continuing with some buildings towards the left, and so on.

The spiral algorithm proves to be more effective in representing the evolution of a project when new elements, such as buildings, emerge and disappear in any place in the hierarchy. In contrast, rectangle packing may cause existing buildings to shift to accommodate new ones, thereby disrupting the perceivable relationship between the buildings' prior and new locations.

\babiaCodeCity utilizes software metrics to map features of the buildings. Each building is associated with a file, and each district corresponds to a directory that can contain files and subdirectories. \babiaCodeCity facilitates the mapping of any metric available to either the base's area, height, or color of each building.

\babiaCodeCity scenes are interactive, enabling users to explore the city using cursor keys in the on-screen mode, and by walking in the physical world in VR. Users can also view data about buildings of interest. In the on-screen mode, hovering the cursor over a building opens a tooltip displaying the file name and metric values such as the number of functions, lines of code, and cyclomatic complexity~\cite{complexity}. In VR, the same feature is facilitated by the raycaster provided by the VR controller, which is the Oculus Quest 2 pointing mechanism.


\figref{fig:diagram} summarizes the complete workflow to produce a scene with \babiaCodeCity starting from a source code snapshot in a \tool{Git} repository.

\begin{figure}[ht]
    \centering
    \includegraphics[width=.95\columnwidth]{img/pipeline.pdf}
    \caption{\babia Workflow: From Source Code to a Scene}
    \label{fig:diagram}
\end{figure}



% Evolution with the timeline
\subsection{Time evolution}

The virtual reality scene of the city features a highly interactive navigation bar, which can be easily accessed and utilized with the virtual reality device's controllers. The navigation bar is prominently displayed on the user interface, as depicted in \figref{fig:navbar}. It consists of a horizontal line, displaying the active time instant, along with a series of interaction buttons placed below to enable users to move forward or backward in time. By default, the navigation bar automatically advances the scene every five seconds, from the past to the future, though this default setting is fully customizable. Users can easily configure the direction and speed of movement between instants using the two vertical bars placed on the right-hand side of the navigation bar. Additionally, users can also make sudden and non-consecutive jumps to any point in time using the buttons below or by simply interacting with the timeline on the navigation bar itself.


As the user interacts with the navigation bar to change the time instant, the city undergoes a seamless transformation, with buildings and quarters gracefully shifting positions to match the new timeframe. To ensure that the user's perception remains uninterrupted, any new structures that appear momentarily become transparent, while disappearing buildings and quarters are promptly removed from view. This fluid motion enables the user to keep a clear and coherent understanding of the evolving city.

\begin{figure}[ht]
    \centering
    \includegraphics[width=\columnwidth]{img/navbar.png}
    \caption{Navigation bar for time evolution}
    \label{fig:navbar}
\end{figure}


The design of this navigation bar has been carefully selected based on common types of navigation bars used for multimedia players, such as video or sound players. The aim of the design is to provide users with a user-friendly and intuitive interface, that is both easy to use and quick to learn. By providing users with these powerful tools to control the evolution of the city, \babiaCodeCity offers a highly interactive and customizable experience that encourages exploration and experimentation with the data.


% JetUML
\subsection{Use case}

In this first work in porgress use case, the scene allows the user to interact with a \codecity visualization of the source code of \jet,\footnote{\jet: \urltt{https://github.com/prmr/JetMUL}}.

\begin{figure}[ht]

    \begin{subfigure}[b]{\columnwidth}
        \centering
        \includegraphics[width=\linewidth]{img/jetumlnew.png}
        \caption{Release 3.2 of \jet}
        \label{fig:jet2021}
    \end{subfigure}

    \vspace{0.5em}

    \begin{subfigure}[b]{\columnwidth}
        \centering
        \includegraphics[width=\linewidth]{img/jetumlold.png}
        \caption{Release 1.0 of \jet}
        \label{fig:jet2018}
    \end{subfigure}
    \caption{The \jet in two different releases}
    \label{fig:cities}
\end{figure}

The \jet city, in all its releases from 1.0 to 3.2, requires the user to utilize the navigation bar and controls of the Quest 2 in order to navigate through its various scenes. By default, the city evolves from past to future automatically, with the layout, buildings, and quarters adjusting accordingly as the displayed release is changed. However, users also have the ability to advance from future to past, as well as configure the speed at which the changes between releases occur. Furthermore, users can make sudden changes between non-consecutive releases, enabling them to compare and contrast between two distant releases of the city. This level of flexibility and interactivity adds to the overall immersive experience and utility of the \jet city.


\textbf{Mapping of metrics.} In both of our experiments we use a mapping of metrics to features of the buildings which are similar to those in the original \codecity implementation. Each building represents a file. The area of its base corresponds to the number of functions (\texttt{num\_funs}), its height to the lines of code per function (\texttt{loc\_per\_function}), and its color the Cyclomatic Complexity Number (\texttt{CCN}, in a blue to red scale. Therefore, the volume of the building represents the number of lines of code (\texttt{LOC}) of the file.


There is a replication package available with all the needed steps to deploy and see this scene~\footnote{\babiareplicationpackage}